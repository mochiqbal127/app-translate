<?php
require_once ('vendor/autoload.php');
use Statickidz\GoogleTranslate;
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Translate App</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-light bg-light justify-content-center">
        <span class="navbar-brand mb-0 h1">Translate</span>
    </nav>
    <br>
    <div class="container">
        <form id="myForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
            <div class="form-group row">
                <select name="source" id="" class="form-control col-sm-5" required>
                    <option value="" disabled>Select Language</option>
                    <option value="en" selected>English</option>
                    <option value="id">Indonesian</option>
                    <option value="es">Spanish</option>
                    <option value="hi">Hindi</option>
                    <option value="ja">Japanese</option>
                    <option value="jv">Javanese</option>
                    <option value="su">Sundanese</option>
                    <option value="ko">Korean</option>
                    <option value="it">Italian</option>
                </select>
                <Input type="text" class="form-control col-sm-2 text-center" disabled value="To">
                <select name="target" id="" class="form-control col-sm-5" required>
                    <option value="" disabled selected>Select Language</option>
                    <option value="en">English</option>
                    <option value="id" selected>Indonesian</option>
                    <option value="es">Spanish</option>
                    <option value="hi">Hindi</option>
                    <option value="ja">Japanese</option>
                    <option value="su">Sundanese</option>
                    <option value="ko">Korean</option>
                    <option value="it">Italian</option>
                </select> 
            </div>
            <div class="form-group row">
                <textarea name="text_source" placeholder="input text here.." class="form-control" rows="5" required></textarea>
            </div>
            <div class="form-group row">
                <button type="submit" name="translate" class="btn btn-primary col-sm-12">Translate</button>
            </div>         
        </form>
        <?php
            if(isset($_POST['translate']) && isset($_POST['text_source']) && isset($_POST['source']) && isset($_POST['target'])){
                $text_src = $_POST['text_source'];
                $src = $_POST['source'];
                $trgt = $_POST['target'];
                
                
                $source = $src;
                $target = $trgt;
                $text = $text_src;
                
                $trans = new GoogleTranslate();
                $result = $trans->translate($source, $target, $text);
        ?>

            <div class="form-group row">
                <b>Translate :</b>
                <textarea style="background : white" class="form-control" readonly rows="5"><?php echo $result; ?></textarea>
            </div>
        <?php
            }

        ?>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script>
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
</body>
</html>
